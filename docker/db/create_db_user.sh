#!/usr/bin/env bash
#
# Database server initialisation script
#
##

echo "Creating user ${MYSQL_USER}:${MYSQL_PASSWORD}"

sed "s/DB_USER/${MYSQL_USER}/;s/DB_PASS/${MYSQL_PASSWORD}/" /config/create_db_user.sql | mysql -uroot -p${MYSQL_ROOT_PASSWORD} mysql
