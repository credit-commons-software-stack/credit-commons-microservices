# Create development MySQL account. Must be run with root credentials.

GRANT ALL PRIVILEGES ON `credcom_%`.* to `DB_USER`@`%` identified by 'DB_PASS';
FLUSH PRIVILEGES;
