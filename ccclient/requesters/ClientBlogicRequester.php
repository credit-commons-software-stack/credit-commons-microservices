<?php

/**
 * Handle requests & responses from the client to the ledger.
 */
class ClientBlogicRequester extends BlogicRequester{
  use ClientRequesterTrait;

  function __construct($other_domain) {
    $this->serviceUrl = str_replace('://', '://blogic.', $other_domain);
  }

  function _appendto(stdClass $transaction) : array {
    list($code, $additional) = $this
      ->setBody($transaction->entries[0])
      ->setMethod('post')
      ->accept(200)
      ->request('append/'.$transaction->type);
    return $additional;
  }

}
