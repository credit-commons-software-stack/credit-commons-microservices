<?php

/**
 * Handle requests & responses from the client to the ledger.
 */
trait ClientRequesterTrait {

  private $print = 0;

  /**
   * This duplicates the whole function in the base class but its just a quick
   * way to avoid sending the hash in the parent class.
   * @param string $endpoint
   * @return mixed
   */
  function request(string $endpoint = '') :array {
    $this->timeout = 4;
    $this->setHeader('Accept', 'application/json');
    if (isset($_GET['acc'])) {
      $this->setHeader('Cc-Enduser', $_GET['acc']);
    }
    else{
      $this->setHeader('Cc-Enduser', '-admin-');
    }
    $this->buildUrl($endpoint);
    if ($this->print) {
      clientAddInfo('Sent '. $this->method ." to $this->fullUrl");
      if (!empty($this->body)) {
        clientAddInfo(json_encode($this->body));
      }
    }
    if ($arr = $this->doCurl($this->method, $this->fullUrl)) {
      list($code, $result_body, $headers) = $arr;
      $result = $this->handleRawResult($code, $result_body, $this->parseheaders($headers));
      if (!is_null($result)) {
        return $this->return($code, $result);
      }
    }
    return $this->return(NULL, []);
  }

  protected function handleRawResult($code, $result_body = NULL, $headers) {
    if (!in_array($code, $this->acceptOnlyCodes)) {
      clientAddError("Unexpected response '$code' from $this->fullUrl");
      clientAddError(htmlentities($result_body));
      return;
    }
    if ($code == 401) {
      clientAddError("Unrecognised user to $this->fullUrl". @$_GET['acc']);
    }
    elseif ($headers['Content-Type'] == 'application/json') {
      $result = json_decode($result_body);
      if ($code == 515 || $code == 500 || $code == 400 and $result) {
        $error = $this->decodeJsonError($result);
        clientAddError('Credit Commons Exception: '. $error);
        // just relay the response back upstream
        return;
      }
      if ($result_body and $result_body != 'null' and is_null($result)) {
        clientAddError("$code Badly formed json received from $this->fullUrl");
        clientAddError("Response body: ".$result_body);
        return;
      }
      return $result;
    }
  }

    /**
   * Reset this Requester, ready for reuse.
   */
  protected function return($code, $result = NULL, $comment = NULL) {
    global $orientation;
    $this->queryParams = [];
    $this->headers = [];
    $this->body = '';
    $this->fields = [];
    $this->acceptOnlyCodes = [];
    // only for file_get_contents.
    $this->print = FALSE;
    return [$code, $result, $comment];
  }

  function print() {
    $this->print = 1;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  protected function doFGC($method) {
    $this->httpOptions['method'] = $this->method;
    $this->httpOptions['timeout'] = $this->timeout;
    if ($this->method != 'GET') {
      $this->httpOptions['content'] = $this->getJsonBody();
    }
    $this->httpOptions['header'] = $this->getHeaders(TRUE);
    $context = stream_context_create(['http' => $this->httpOptions]);
    // file_get_contents is a bit crude for http but has no dependencies
    $result_body = file_get_contents($this->fullUrl, false, $context);
    $this->httpOptions = ['method' => 'GET', 'ignore_errors' => TRUE]; // for next time
    if (empty($http_response_header)) {
      clientAddError($this->httpOptions);
      clientAddError("No response from ".$this->method." $this->fullUrl");
      return;
    }
    // in case of redirects, get the last header starting HTTP/1.1
    foreach (array_reverse($http_response_header) as $line) {
      if (preg_match('/^HTTPS?\/1\.[0|1] ([0-9]{3}) ?(.*)$/', $line, $matches)) {
        $code = $matches[1];
        break;
      }
    }
    if (!isset($code)) {
      clientAddError('Unable to parse response headers: ' . print_r($http_response_header, 1));
      return;
    }
    elseif ($code == 405) {
      // This happens when the vhost doesn't exist
      clientAddError("405 error - Did you restart the web server");
      return;
    }
    return [$code, $result_body, $http_response_header];
  }

  /**
   * {@inheritDoc}
   */
  protected function doCurl($method) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $this->fullUrl);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders(FALSE));
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    // This technique taken from https://stackoverflow.com/a/9434158
    // But only gets to the $_COOKIE in the second query;
    //curl_setopt($ch, CURLOPT_COOKIE, 'sessionid='.session_id());
    //cc_log("Sending cookie to $this->fullUrl with sessionid: ".session_id());
    if ($method <> 'GET') {
      if ($method == 'POST') {
        curl_setopt($ch, CURLOPT_POST, 1);
      }
      else{
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
      }
      curl_setopt($ch, CURLOPT_POSTFIELDS, $this->getJsonBody());
    }
    $response = curl_exec($ch);
    $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    if (empty($code)) {
      clientAddError(curl_getinfo($ch));
      clientAddError("No response from ".$this->method." $this->fullUrl");
      return [400, NULL, '', []];
    }
    elseif ($code == 405) {
      // This happens when the vhost doesn't exist
      clientAddError("405 error - Is the vhost present in the server config?");
      return [NULL, NULL, NULL, []];
    }
    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    curl_close($ch);
    return [
      $code,
      substr($response, $header_size),
      explode("\n", substr($response, 0, $header_size))
    ];
  }

}
