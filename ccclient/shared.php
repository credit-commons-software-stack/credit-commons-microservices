<?php
ini_set('display_errors', 1);
require_once "../node_template/common.php";
require_once '../node_template/ledgerService/db.php';
require_once "Node.php";

global $nodes;
$nodes = Node::loadAll();
@unlink('../devel.log');

/**
 *
 * @global string $info
 * @param mixed $message
 */
function clientAddError($message) {
  global $info;
  if (!is_string($message)) {
    $message = '<pre>'.print_r($message, 1).'</pre>';
  }
  $message = str_replace('BoT', '<span title="Balance of Trade account">BoT</span>', $message);
  $info[] = '<font color="red">'.$message.'</font>';
}

/**
 *
 * @global string $info
 * @param mixed $message
 */
function clientAddInfo($message) {
  global $info;
  if ($message) {
    if (!is_string($message)) {
      $message = '<pre>'.print_r($message, 1).'</pre>';
    }
    $message = str_replace('BoT', '<span title="Balance of Trade account">BoT</span>', $message);
    $info[] = '<font color="green">'.$message.'</font>';
  }
}