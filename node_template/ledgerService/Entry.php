<?php

/**
 * This class pretty much mirrors what's in the database. But also translates
 *  payer and payee addresses for upstream/downstream ledgers.
 *
 */
class Entry implements JsonSerializable {

  /**
   * @var Account
   */
  public $payee;

  /**
   * @var Account
   */
  public $payer;

  /**
   * @var Account
   */
  public $author;

  /**
   * @var Int
   */
  public $quant;

  /**
   * @var string
   */
  public $description;

  /**
   * @var stdclass
   */
  public $metadata;

  /**
   * @var array
   */
  public $isPrimary;

  /**
   * @param LedgerAccount $payee
   * @param LedgerAccount $payer
   * @param string $description
   * @param float $quant
   * @param string $author
   * @param bool $is_primary
   * @param stdClass $metadata
   */
  function __construct(LedgerAccount $payee, LedgerAccount $payer, string $description, float $quant, string $author, $is_primary, stdClass $metadata = NULL) {
    $this->payee = $payee;
    $this->payer = $payer;
    $this->description = $description??'';
    $this->quant = $quant;
    $this->author = $author;
    $this->metadata = $metadata ?? new stdClass;
    $this->isPrimary = $is_primary;
  }

  /**
   * Convert the account names to Account objects, and instantiate the right entry object class.
   *
   * @param object $row
   *   Could be from client or a flattened Entry
   * @param bool $existing
   *   TRUE if the transaction has already been written, and thus we know the accounts exist.
   * @return \Entry
   */
  static function create(stdClass $row, bool $existing) : Entry {
    $payee = LedgerAccount::create($row->metadata->{$row->payee} ?? $row->payee, $existing);
    $payer = LedgerAccount::create($row->metadata->{$row->payer} ?? $row->payer, $existing);

    //unknown accounts will show up as the balance of trade account.
    if ($payer->localName == $payee->localName) {
      cc_violation(SameAccountViolation::create($payer->localName, $row->description));
    }
    foreach (['payee', 'payer'] as $role) {
      if ($$role instanceOf LedgerAccountRemote) {
        $row->metadata->{$$role->localName} = $$role->givenPath;
      }
    }
    $class = static::determineClass($payee, $payer);

    return new $class(
      $payee,
      $payer,
      $row->description,
      $row->quant,
      $row->author,
      $row->isPrimary,
      $row->metadata
    );
  }

  public static function determineClass($acc1, $acc2) {
    $class = 'Entry';
    // Now, depending on which of those vars are set, we determine the class of this entry.
    if ($acc1 instanceOf LedgerAccountBranch and $acc2 instanceOf LedgerAccountBranch) {
      $class = 'TransversalEntry';
    }
    elseif ($acc1 instanceOf LedgerAccountBoT or $acc2 instanceOf LedgerAccountBoT) {
      $class = 'RootwardsEntry';
    }
    elseif ($acc1 instanceOf LedgerAccountBranch or $acc2 instanceOf LedgerAccountBranch) {
      $class = 'TransversalEntry';
    }

    return $class;
  }

  /**
   * Entries shared locally
   * if its for a local_request between services
   * - Account names collapsed to local name
   * or if its going back to the client
   * - Account names collapsed to a relative name
   * - Quant rounded to go back to the client
   * @return stdClass
   */
  public function jsonSerialize() : array {
    global $config, $orientation;
    $flat = [
      'payee' => $this->payee->localName,
      'payer' => $this->payer->localName,
      'author' => $this->author,
      'quant' => $this->quant,
      'description' => $this->description
    ];
    if (!$orientation->localRequest) {// going back to client.
      $flat['payee'] = $this->metadata->{$flat['payee']} ?? $flat['payee'];
      $flat['payer'] = $this->metadata->{$flat['payer']} ?? $flat['payer'];
      $flat['quant'] = round($this->quant, $config['decimal_places']);
    }
    return $flat;
  }

  /**
   * TRUE if this entry was authored locally or downstream.
   * @var bool
   */
  private $additional;

  function additional() {
    $this->additional = TRUE;
    return $this;
  }

  function isAdditional() {
    return $this->additional;
  }

}
