<?php

/**
 * Class representing an account corresponding to an account on another ledger
 */
class LedgerAccountBoT extends LedgerAccountRemote {

  function transversalPath() : string {
    return $this->givenPath;
  }
}

final class LedgerAccountUpstreamBoT extends LedgerAccountBoT {

}

final class LedgerAccountDownstreamBoT extends LedgerAccountBoT {

}


