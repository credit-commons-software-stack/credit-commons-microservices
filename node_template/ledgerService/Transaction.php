<?php

/**
 * A transaction consists of several ledger entries sharing a common UUID, type and workflow state.
 * There is a primary entry, and many dependents.
 * Entries can be either transversal or local.
 * The transaction is either transversal or local, depending on the primary entry.
 * That means a local transaction cannot have transversal dependents.
 */
class Transaction implements TransactionInterface, JsonSerializable {

  /**
   * Universally Unique Identifier
   * @var string
   */
  public $uuid;

  /**
   * Database ID used to join the transaction table to the entries table
   * @var int
   */
  public $txID;

  /**
   * Version of the transaction, incremented by one each save.
   * @var int
   */
  public $version;

  /**
   * The workflow path of the transaction. Converted to a hash for transversal
   * transactions
   * @var string
   */
  public $type;

  /**
   * The current workflow state of this transaction.
   * @var string
   */
  public $state;

  /**
   * Objects containing the payer, payee description etc which do not change with the workflow
   * @var Entry[]
   */
  public $entries = [];

  /**
   * The workflow object for this transaction, determined by the $type.
   * @var Workflow
   */
  public $workflow;

  /**
   * @param string $uuid
   * @param int $version
   * @param string $type
   * @param string $state
   * @param Entry[] $entries
   */
  public function __construct(string $uuid, int $version, string $type, string $state, array $entries, $txID = NULL) {
    $this->uuid = $uuid;
    $this->version = $version;
    $this->workflow = Workflow::create($type, $this);
    $this->type = $this->workflow->id;
    $this->state = $state;
    $this->entries = $entries;
    $this->txID = $txID;
  }

  /**
   * Create a new transaction from the essential data supplied by the client.
   * @param stdClass $input
   *   validated to contain payer, payee, description & quantity
   * @return \static
   */
  public static function createFromClient(stdClass $input) : Transaction {
    global $config;
    // basic validation of the input
    if (!isset($input->quant) or (!$config['zero_payments'] and empty($input->quant))) {
      $missing[] = 'quant';
    }
    foreach (['payer', 'payee', 'description'] as $field_name) {
      // @todo there is a setting that allows 'quant' to be empty;
      if (!isset($input->{$field_name}) or empty($input->{$field_name})) {
        $missing[] = $field_name;
      }
    }
    if ($missing) {
      cc_violation(MissingRequiredFieldViolation::create($missing));
    }
    $input->author = $_SESSION['user'];
    $entries = static::createEntries([$input], FALSE);
    $class = static::determineClass($entries);

    return new $class(
      static::makeUuid(),
      0,
      $input->type??$config['default_transaction_type'],
      TransactionInterface::STATE_INITIATED,
      $entries
    );
  }

  /**
   * Build this set, making sure that the primary transaction is the first.
   *
   * @param stdClass[] $rows
   *   Which are  Entry objects flattened by json for transport.
   * @param bool $existing
   *   TRUE if the transaction has already been written, and thus we know the accounts exist.
   * @return Entry[]
   *   The created entries
   */
  static function createEntries(array $rows, bool $existing) : array {
    asort($rows, function ($a, $b){return isset($a->isPrimary);});
    $rows[0]->isPrimary = TRUE;
    foreach ($rows as $row) {
      if (empty($row->author)) {
        trigger_error('Entry is missing author.', E_USER_WARNING);
      }
      $entries[] = Entry::create($row, $existing);
    }
    return $entries;
  }

  /**
   * @param array $entries
   * @return boolean
   *   TRUE if these entries imply a TransversalTransaction
   */
  private static function determineClass(array $entries) : string {
    foreach ($entries as $entry) {
      if ($entry instanceOf TransversalEntry) {
        include_transversal_classes();
        return 'TransversalTransaction';
      }
    }
    return 'Transaction';
  }

  /**
   * @param type $uuid
   * @return \Transaction
   */
  static function loadByUuid($uuid) : Transaction {
    global $orientation;
    $q = "SELECT id, version, type, state FROM transactions "
      . "WHERE uuid = '$uuid' "
      . "ORDER BY version DESC "
      . "LIMIT 0, 1";
    $tx = Db::query($q)->fetch_object();
    if ($tx) {
      $q = "SELECT payee, payer, description, quant, author, metadata FROM entries "
        . "WHERE txid = $tx->id "
        . "ORDER BY id ASC";
      $result = Db::query($q);
      while ($row = $result->fetch_object()) {
        $row->metadata = unserialize($row->metadata);
        $entry_rows[] = $row;
      }
      $entries = static::createEntries($entry_rows, TRUE);
      $class = static::determineClass($entries);
      $transaction = new $class(
        $uuid,
        $tx->version,
        $tx->type,
        $tx->state,
        // The two booleans here don't matter since the transaction isn't being transported
        $entries,
        $tx->id
      );
    }
    else {
      $transaction = static::getTemp($uuid);
      $orientation->addAccount($transaction->entries[0]->payee);
      $orientation->addAccount($transaction->entries[0]->payer);
    }
    return $transaction;
  }

  /**
   * Write the serialized transaction to the temp table.
   * @return bool
   *   TRUE on success
   */
  function writeValidatedToTemp() {
    //version is 0 until is it written in the transactions table.
    $data = Db::connect()->real_escape_string(serialize($this));
    $q = "INSERT INTO temp (uuid, serialized) VALUES ('$this->uuid', '$data')";
    $result = Db::query($q);
    cc_message('Written validated transaction '.$this->uuid. ' to temp table');
    return (bool)$result;
  }


  /**
   * Call the business logic and append entries.
   */
  function buildValidate() : void {
    $this->workflow->canSign(Transaction::STATE_VALIDATED);
    // Add fees, etc by calling on the blogic service
    $fees = BlogicRequester::create()->appendTo($this);
    // @todo. Validate these since they came from another microservice
    foreach ($fees as $row) {
      $this->entries[] = Entry::create($row, FALSE)->additional();
    }
    foreach ($this->sum() as $localName => $info) {
      $account = LedgerAccount::create($localName, FALSE);
      $ledgerAccountInfo = $account->getTradeStats();
      $projected = $ledgerAccountInfo['pending']['balance'] + $info->diff;
      if ($projected > $this->payee->max) {
        cc_violation(MaxLimitViolation::create($localName, $this->payee->max, $projected));
      }
      elseif ($projected < $this->payer->min) {
        cc_violation(MinLimitViolation::create($localName, $this->payer->min, $projected));
      }
    }
    $this->state = TransactionInterface::STATE_VALIDATED;
  }

  /**
   * @param Transaction $transaction
   * @param string $target_state
   * @throws \Exception
   */
  function changeState(string $target_state) {
    cc_message("Changing the state of the transaction to $target_state");
    $this->sign($target_state);
  }

  function sign($target_state) {
    $this->workflow->canSign($target_state);
    $this->state = $target_state;
    $this->version++;
    $this->saveTransactionNewVersion();
    return $this;
  }

  /**
   * Write the transaction entries to the database.
   *
   * @note No database errors are anticipated.
   */
  public function saveTransactionNewVersion() {
    // The datestamp is added automatically
    $acc = $_SESSION['user'];
    $q = "INSERT INTO transactions (uuid, version, type, state, scribe) "
    . "VALUES ('$this->uuid', $this->version, '$this->type', '$this->state', '$acc')";
    $new_id = Db::query($q);
    $this->writeEntries($new_id);
  }

  protected function writeEntries($new_txid) {
    if ($this->txID) {// this transaction has already been written in an earlier state
      $q = "UPDATE entries set txid = $new_txid WHERE txid = $this->txID";
      Db::query($q);
    }
    else {// this is the first time the transaction is written properly
      foreach ($this->entries as $entry) {
        $this->insertEntry($new_txid, $entry);
      }
      Db::query("DELETE FROM temp WHERE uuid = '$this->uuid'");
    }
    cc_message('Written transaction');
  }

  /**
   * Save an entry to the entries table.
   * @param int $txid
   * @param Entry $entry
   * @return int
   *   the new entry id
   * @note No database errors are anticipated.
   */
  private function insertEntry(int $txid, Entry $entry) : int {
    $payee = $entry->payee->prepareWrite($entry->metadata);
    $payer = $entry->payer->prepareWrite($entry->metadata);
    $metadata = serialize($entry->metadata);
    $primary = intval($entry->isPrimary);
    $desc = Db::connect()->real_escape_string($entry->description);
    $q = "INSERT INTO entries (txid, payee, payer, quant, description, author, metadata, is_primary) "
      . "VALUES ($txid, '$payee', '$payer', '$entry->quant', '$desc', '$entry->author', '$metadata', '$primary')";
    if ($this->id = Db::query($q)) {
      return (bool)$this->id;
    }
  }

  /**
   * Retrieve a transaction from serialized, in the db.
   * @param string $uuid
   * @return \Transaction
   */
  protected static function getTemp($uuid) : Transaction {
    $result = Db::query("SELECT serialized FROM temp WHERE uuid = '$uuid'");
    if ($stored = $result->fetch_object()) {
      // Might have to undo real_escape_string
      if ($string = $stored->serialized) {
        //Need to ensure the class files are included i.e. transversalTransaction and ledger accounts;
        preg_match_all('/O:[0-9]+:"([a-zA-Z]+)/', $string, $matches);
        foreach (array_unique($matches[1]) as $class) {
          if ($class != 'Entry' and $class != 'Transaction') {
            include_transversal_classes();
            //easiest just to include them all here
            require_once 'Transversal/LedgerAccountBranch.php';
            require_once 'Transversal/LedgerAccountBoT.php';
            break;
          }
        }
        $transaction = unserialize($string);
        return $transaction;
      }
    }
    cc_violation(DoesNotExistViolation::create($uuid));
  }

  /**
   * Magic method. Look for any unknown properties to the first entry.
   * @param string $name
   * @return type
   */
  function __get($name) {
    $valid = ['payee', 'payer', 'description'];
    if (isset($this->entries[0]->$name)) {
      return $this->entries[0]->$name;
    }
    cc_failure(MiscFailure::create('Requested unknown property of Transaction:'.$name));
  }

  /**
   * Add up all the transactions and return the differences in balances for
   * every involved user.
   *
   * @param Transaction $transaction
   * @return array
   *   The differences, keyed by account name
   */
  public function sum() : array {
    $accounts = [];
    foreach ($this->entries as $entry) {
      $accounts[$entry->payee->localName] = $entry->payee;
      $accounts[$entry->payer->localName] = $entry->payer;
      $sums[$entry->payer->localName][] = -$entry->quant;
      $sums[$entry->payee->localName][] = $entry->quant;
    }
    foreach ($sums as $localName => $diffs) {
      $accounts[$localName]->diff = array_sum($diffs);
    }
    return $accounts;
  }

  /**
   * Send the transaction back to the client.
   * - get the actions
   * - remove some properties.
   *
   * @return type
   */
  public function jsonSerialize() : array {
    $array = (array)$this;
    $array['actions'] = $this->workflow->getActions();
    unset($array['version']);
    unset($array['workflow']);
    unset($array['txID']);
    return $array;
  }

  /**
   * @param array $params
   *   valid keys: state, payer, payee, involving, type, before, after, description
   * @return uuid[]
   * @note Because neither signatures as such nor the need for them is stored in
   * the db, this method can't filter by them.
   */
  static function filter(array $params) : array {
    extract($params);
    $query = "SELECT t.uuid FROM transactions t "
      . "INNER JOIN versions v ON t.uuid = v.uuid AND t.version = v.ver "
      . "LEFT JOIN entries e ON t.id = e.txid";
    if (isset($is_primary) and $is_primary) {
      // This prevents you from filtering for non-primary transactions only.
      $conditions[]  = 'is_primary = 1';
    }
    if (isset($payer)) {
      if ($col = strpos($payer, '/')) {
        $conditions[] = "metadata LIKE '%$payer%'";
      }
      else {
        // At the moment metadata only stores the real address of remote parties.
        $conditions[]  = "payer = '$payer'";
      }
    }
    if (isset($payee)) {
      if ($col = strpos($payee, '/')) {
        $conditions[] = "metadata LIKE '%$payee%'";
      }
      else {
        // At the moment metadata only stores the real address of remote parties.
        $conditions[]  = "payee = '$payee'";
      }
    }
    if (isset($author)) {
      $conditions[]  = "author = '$author'";
    }
    if (isset($involving)) {
      if ($col = strpos($involving, '/')) {
        $conditions[] = "( metadata LIKE '%$payer%'";
      }
      else {
        // At the moment metadata only stores the real address of remote parties.
        $conditions[]  = "(payee = '$involving' OR payer = '$involving')";
      }
    }
    if (isset($description)) {
      $conditions[]  = "description LIKE '%$description%'";
    }
    if (isset($before)) {
      $date = date("Y-m-d H:i:s", strtotime($before));
      $conditions[]  = "written < '$date'";
    }
    if (isset($after)) {
      $date = date("Y-m-d H:i:s", strtotime($after));
      $conditions[]  = "written > '$date'";
    }
    if (isset($state)) {
      $conditions[]  = "state = '$state'";
    }
    if (isset($type)) {
      $conditions[]  = "type = '$type'";
    }
    if (isset($uuid)) {
      $conditions[]  = "t.uuid = '$uuid'";
    }
    if (isset($conditions)) {
      $query .= ' WHERE '.implode(' AND ', $conditions);
    }
    $result = Db::query($query);
    $uuids = [];
    while ($row = $result->fetch_object()) {
      $uuids[] = $row->uuid;
    }
    return $uuids;
  }

  /**
   * Make a standard random universal unique identifier
   * @return string
   */
  private static function makeUuid() : string{
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand(0, 0xffff),
      mt_rand(0, 0xffff),
      mt_rand(0, 0xffff),
      mt_rand(0, 0x0fff) | 0x4000,
      mt_rand(0, 0x3fff) | 0x8000,
      mt_rand(0, 0xffff),
      mt_rand(0, 0xffff),
      mt_rand(0, 0xffff)
    );
  }
}


interface TransactionInterface {
  const STATE_INITIATED = 'init'; //internal use only
  const STATE_VALIDATED = 'validated';
  const STATE_PENDING = 'pending';
  const STATE_COMPLETED = 'completed';
  const STATE_ERASED = 'erased';
  const STATE_TIMEDOUT = 'timedout';
}
