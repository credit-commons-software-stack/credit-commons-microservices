<?php


/**
 * These violations are passed as objects to be recreated by the client. Then
 * the __toString() can provide localised error messages.
 */


abstract class TransactionLimitViolation extends CCError {
  public $acc_id;
  public $limit;
  public $projected;

  /**
   * @param string $acc_id
   *   The account name whose limits would be exceeded
   * @param string $limit
   *   The amount of the limit which would be exceeded
   * @param string $projected
   *   The projected balance
   */
  static function create(...$params) :  ErrorInterface {
    return new static(['acc_id' => $params[0], 'limit' => $params[1], 'projected' => $params[2]]);
  }
}

/**
 * Pass $account_name, $min_limit, $projected_balance
 */
final class MinLimitViolation extends TransactionLimitViolation {
  function __toString() {
    $this->diff = $this->limit - $this->projected;
    return "This transaction would put $this->acc_id $this->diff below the minium balance on $this->node";
  }
}

/**
 * Pass $account_name, $max_limit, $projected_balance
 */
final class MaxLimitViolation extends TransactionLimitViolation {
  function __toString() {
    $this->diff = $this->limit - $this->projected;
    return "This transaction would put $this->acc_id $this->diff above the maxium balance on $this->node";
  }
}

/**
 * Pass $operation description, $account_name (optional)
 */
class PermissionViolation extends CCError {
  public $account;
  public $message;

  /**
   * @param string $message
   * @param string $account
   */
  static function create(...$params) :  ErrorInterface  {
    return new static(['message' => $params[0], 'account' => $params[1]??$_SESSION['user']]);
  }
  function __toString() {
    return "On $this->node $this->account is not permitted to: ".$this->message;
  }
}

class WorkflowViolation extends CCError {
  public $uuid;
  public $target_state;
  public $account;
  public $type;

  /**
   * @param string $uuid
   * @param string $target_state
   * @param string $typ
   * @param string $account
   */
  static function create(...$params) :  ErrorInterface  {
    return new static([
      'uuid' => $params[0],
      'target_state' => $params[1],
      'type' => $params[2],
      'account' => $_SESSION['user']
    ]);
  }
  function __toString() {
    return "On node $this->node, workflow '$this->type' prevents account $this->account from moving transaction to $this->target_state.";
  }
}
class UnknownWorkflowViolation extends CCError {
  public $id;

  /**
   * @param string $id
   */
  static function create(...$params) :  ErrorInterface  {
    return new static(['id' => $params[0]]);
  }
  function __toString() {
    return "No workflow on $this->node corresponds to the id $this->id";
  }
}

final class DoesNotExistViolation extends CCError {
  public $uuid;

  /**
   * @param string $uuid
   */
  static function create(...$params) :  ErrorInterface  {
    return new static(['uuid' => $params[0]]);
  }

  function __toString() {
    return "The transaction with the uuid $id does not exist";
  }
}
final class HashMismatchViolation extends CCError {
  public $node_name;

  /**
   * @param string $node_name
   */
  static function create(...$params) :  ErrorInterface  {
    return new static(['node_name' => $params[0]]);
  }

  function __toString() {
    return "The ledger ratchet with $this->node_name is broken ";
  }
}

final class IntermediateledgerViolation extends CCError {

  function __toString() {
    return "Transversal transactions cannot be manipulated via intermediate ledgers";
  }
}

final class UnresolvedAccountnameViolation extends CCError {
  public $path;

  /**
   * @param string  $path
   */
  static function create(...$params) :  ErrorInterface {
    return new static(['path' => $params[0]]);
  }
  function __toString() {
    return "$this->node could not find an account that matched '$this->path'";
  }
}

final class SameAccountViolation extends CCError {
  public $name;
  public $description;

  /**
   * @param string $name
   * @param string $description
   */
  static function create(...$params) :  ErrorInterface {
    return new static(['name' => $params[0], 'description' => $params[1]]);
  }
  function __toString() {
    return "The payer and payee names both resolved to the same local account, $this->name for the transaction '$this->description'.";
  }
}

final class MissingRequiredFieldViolation extends CCError {
  public $fieldnames;

  /**
   * @param string $fieldnames
   */
  static function create(...$params) :  ErrorInterface {
    return new static(['fieldnames' => $params[0]]);
  }
  function __toString() {
    return "Must not be empty: ".implode(', ', $fields);
  }
}

final class MissingWorkflowError extends CCError {
  function __toString() {
    return "No workflows available";
  }
}

final class LedgerOrientationFailure extends CCError {
  // This should never happen
  function __toString() {
    return 'Please report a LedgerOrientationFailure to your system administrator';
  }
}

final class MissingPolicyFailure extends CCError {
  public $name;

  /**
   * @param string $name
   */
  static function create(...$params) :  ErrorInterface {
    return new static(['name' => $params[0]]);
  }
  function __toString() {
    return "Could not retrieve policy for $this->name";
  }
}