<?php

/**
 * Class representing a member
 */
class LedgerAccount {


  //Also possible: partners_global
  const TRADE_STATS = ['entries', 'partners_local', 'trades', 'gross_in', 'gross_out', 'balance', 'volume'];

  /**
   * The name which would be written in the ledger
   * @var string
   */
  public $localName = '';

  /**
   * The path to this account, starting with this node name.
   * @var string
   */
  public $relative = '';

  /**
   * The path being searched for
   * @var string
   */
  public $givenPath = '';

  /**
   * Copied from the policy object, converted from json
   */
  public $min = '';
  public $max = '';
  public $created = '';

  /**
   *
   * @param stdClass $policy
   *   Converted json from the PolicyService Account class
   * @param string $given_path
   *   Unresolved version of the address
   */
  function __construct(stdClass $policy, string $given_path) {
    global $node_name;
    $this->localName = $policy->name; // @todo get rid of localname
    $this->min = $policy->min;
    $this->max = $policy->max;
    $this->created = $policy->created;
    $this->relative = $node_name .'/'.  $this->localName;
    $this->givenPath = $given_path;
    $this->url = $policy->url;
    $this->ip = $policy->ip;
  }

  /**
   * Create a Ledger account object.
   *
   * Choose the class according to whether it is local, branchwards, or bot_account,
   * note that $loaded accounts is initially populated in checkUpstreamHash()
   *
   * @param string $given_path
   * @param bool $existing
   *   TRUE if the transaction has already been written, and thus we know the accounts exist.
   * @return \LedgerAccount
   */
  static function create(string $given_path, bool $existing) : LedgerAccount {
    global $loadedAccounts, $orientation;
    if (!isset($loadedAccounts[$given_path])) {
      $policy = static::resolveAddress($given_path, $existing);
      $class = static::determineClass($policy, $given_path);
      $new_account = new $class($policy, $given_path);
      $orientation->addAccount($new_account);
      $loadedAccounts[$given_path] = $new_account;
    }
    return $loadedAccounts[$given_path];
  }


  /**
   * Determine the class of the Account, taking care to distinguish between
   * remote accounts and their local proxies.
   *
   * @param stdClass $policy
   * @param string $given_path
   * @return string
   */
  static function determineClass(stdClass $policy, string $given_path) : string {
    global $orientation;
    global $config;
    if (isset($policy->url) and $given_path <> $policy->name) {
      include_transversal_classes();
      if ($bot = $policy->name == $config['bot_account']) {
        require_once 'Transversal/LedgerAccountBoT.php';
      }
      else {
        require_once 'Transversal/LedgerAccountBranch.php';
      }
      // this could be refactored into an $orientation method like ->nameIsUpstream($policy->name)
      $ups = ($orientation->upstreamAccount and $orientation->upstreamAccount->localName == $policy->name);
      if ($bot && $ups) {
        $class = 'LedgerAccountUpstreamBoT';
      }
      elseif ($bot && !$ups) {
        $class = 'LedgerAccountDownstreamBoT';
      }
      elseif (!$bot && $ups) {
        $class = 'LedgerAccountUpstreamBranch';
      }
      elseif (!$bot && !$ups) {
        $class = 'LedgerAccountDownstreamBranch';
      }
    }
    else {
      $class = 'LedgerAccount';
    }
    return $class;
  }

  /*
   * Get the name to pass back to the previous ledger or client.
   */
  function transversalPath() : string {
    if ($this->isLocal()){
      return $this->relative;
    }
    else {
      return $this->relative;
    }
  }

  /**
   *
   */
  function isLocal() : bool {
    return !($this instanceof LedgerAccountRemote);
  }

  /**
   *
   * @param stdClass $metadata
   * @return string
   */
  public function prepareWrite(stdClass &$metadata) : string {
    return $this->localName;
  }


  /**
   *
   * @param LedgerAccount $account
   * @param mixed $samples
   *   NULL means just return the raw points. 0 means show a true time record
   *   with a stepped appearance. otherwise return the number of points to smooth to
   *   steps, points, smoothed = .
   * @return array
   *   Balances keyed by timestamp, oldest first
   */
  function getHistory($samples = 0) : array {
    cc_log($this->localName);
    $points[date("Y-m-d H:i:s", $this->created)] = 0;
    $points += $this->_getHistory();
    if ($samples === 0){
      $times = $values = [];
      // Make two values for each one in the keys and values.
      foreach ($points as $time => $bal) {
        $secs = strtotime($time);
        $times[] = date("Y-m-d H:i:s", $secs);
        $times[] = date("Y-m-d H:i:s", $secs+1);
        $values[] = $bal;
        $values[] = $bal;
      }
      // Now slide the arrays against each other to create steps.
      array_shift($times);
      array_pop($values);
      unset($points);
      $points = array_combine($times, $values);
    }
    elseif($samples) {
      //
    }
    if (!$samples and $points) {
      // Finish the line from the last transaction until now.
      $points[date("Y-m-d H:i:s")] = end($points); //this date format corresponds to the mysql DATETIME
      // Note that since the first point is ALWAYS the first transaction in this
      // implementation, we don't create a create a point for initial 0 balance.
    }
    return $points;
  }

  /**
   * @return array
   *   Balances keyed by timestamp, oldest first
   * @todo urgent this reads all versions transactions as different transactions
   */
  private function _getHistory()  : array {
    global $config;
    $chart = [];
    Db::query("SET @csum := 0");
    $query = "SELECT written, (@csum := @csum + diff) as balance FROM transaction_index WHERE uid1 = '$this->localName' ORDER BY written ASC";
    $result = Db::query($query);
    // At the moment we don't know when the account was opened.
    while($t = $result->fetch_object()) {
      $chart[$t->written] = round($t->balance, $config['decimal_places']);
    }
    return $chart;
  }


  /**
   * @return array
   *   Two groups of stats, with keys 'completed' and 'pending'.
   */
  function getTradeStats() : array {
    $query = "SELECT uid2, income, expenditure, diff, volume, state, is_primary as isPrimary "
      . "FROM transaction_index "
      . "WHERE uid1 = '$this->localName'";
    $result = Db::query($query);
    $vals = [
      'balance' => 0,
      'trades' => 0,
      'entries' => 0,
      'volume' => 0,
      'gross_in' => 0,
      'gross_out' => 0
    ];
    $stats = ['completed' => $vals, 'pending' => $vals];
    $pending_partners = $completed_partners = [];
    while ($row = $result->fetch_object()) {
      $stats['pending']['balance'] += $row->diff;
      $stats['pending']['gross_in'] += $row->income;
      $stats['pending']['gross_out'] += $row->expenditure;
      $stats['pending']['volume'] += $row->volume;
      $stats['pending']['entries']++;

      if ($row->state == 'completed') {
        $stats['completed']['balance'] += $row->diff;
        $stats['completed']['gross_in'] += $row->volume;
        $stats['completed']['gross_out'] += $row->expenditure;
        $stats['completed']['volume'] += $row->volume;
        $stats['completed']['entries']++;
      }
      if ($row->isPrimary) {
        $stats['pending']['trades']++;
        $pending_partners[] = $row->uid2;
        if ($row->state == 'completed') {
          $stats['completed']['trades']++;
          $completed_partners[] = $row->uid2;
        }
      }
    }
    $stats['completed']['partners'] = count(array_unique($completed_partners));
    $stats['pending']['partners'] = count(array_unique($pending_partners));
    return [$this->relative, $stats];
  }

  /**
   *
   * @param bool $details
   * @return array
   */
  static function getAllTradeStats(bool $details = FALSE) : array {
    $all_account_names = PolicyRequester::create()->filter(['status' => TRUE], 'nameonly');
    $results = [];
    foreach (static::TRADE_STATS as $stat) {
      $default[$stat] = 0;
    }
    if ($details) {
      // NB this is only the balances of accounts which have traded.
      $all_balances = static::_getAllTradeStats();
      foreach ($all_account_names as $name) {
        $results[$name] = $all_balances[$name]['completed'] ?? (object)$default;
      }
    }
    else {
      $results = $all_account_names;
    }
    return $results;
  }

  /**
   * @return array
   */
  private static function _getAllTradeStats() : array {
    $balances = [];
    $result = Db::query("SELECT uid1, uid2, income, expenditure, diff, volume, state, is_primary FROM transaction_index WHERE income > 0");
    while ($row = $result->fetch_object()) {
      $balances[$row->uid1]['pending']->gross_in[] = $row->income;
      $balances[$row->uid2]['pending']->gross_out[] = $row->income;
      if ($row->is_primary) {
        $balances[$row->uid1]['pending']->partners_local[] = $row->uid2;
        $balances[$row->uid2]['pending']->partners_local[] = $row->uid1;
        $balances[$row->uid1]['pending']->trades[] = 1;
        $balances[$row->uid2]['pending']->trades[] = 1;
      }
      if ($row->state == 'completed') {
        $balances[$row->uid1]['completed']->gross_in[] = $row->income;
        $balances[$row->uid2]['completed']->gross_out[] = $row->income;
        if ($row->is_primary) {
          $balances[$row->uid1]['completed']->partners_local[] = $row->uid2;
          $balances[$row->uid2]['completed']->partners_local[] = $row->uid1;
          $balances[$row->uid1]['completed']->trades[] = 1;
          $balances[$row->uid2]['completed']->trades[] = 1;
        }
      }
    }
    foreach ($balances as &$states) {
      foreach ($states as &$data) {
        foreach (static::TRADE_STATS as $stat) {
          switch ($stat) {
            case 'entries':
              $val = count($data->partners_local);
              break;
            case 'partners_local':
              $val = count(array_unique($data->partners_local));
              break;
            case 'trades':
              $val = count($data->trades);
              break;
            case 'gross_in':
              $val = static::getFormatted(array_sum($data->gross_in));
              break;
            case 'gross_out':
              $val = static::getFormatted(array_sum($data->gross_out));
              break;
            case 'balance':
              $val = static::getFormatted($data->gross_in - $data->gross_out);
              break;
            case 'volume':
              $val = static::getFormatted($data->gross_in + $data->gross_out);
              break;
          }
          $data->{$stat} = $val;
        }
      }
    }
    return $balances;
  }

  private static function getFormatted($num = 0) {
    global $config;
    return round($num, $config['decimal_places']);
  }

  /**
   * Resolve a path to an account on the current node.
   * @return stdClass
   *   The Policy of that account
   * @param bool $existing
   *   TRUE if the transaction has already been written, and thus we know the accounts exist.
   */
  static function resolveAddress(string $given_path, bool $existing) : stdClass {
    global $node_name, $orientation, $config;
    // if its one name and it exists on this ledger then good.
    $parts = explode('/', $given_path);
    if (count($parts) == 1) {
      if ($pol = fetch_policy($given_path, 'full', $existing)) {
        return $pol;
      }
      cc_violation(UnresolvedAccountnameViolation::create($given_path));
    }

    // A branchwards account, including the local node name
    $pos = array_search($node_name, $parts);
    if ($pos !== FALSE and $branch_name = $parts[$pos+1]) {
      if ($pol = fetch_policy($branch_name, 'full', FALSE)) {
        return $pol;
      }
      cc_violation(UnresolvedAccountnameViolation::create($given_path));
    }
    // A branchwards or rootwards account, starting with the account name on the local node
    $branch_name = reset($parts);
    if ($pol = fetch_policy($branch_name, 'full', FALSE)) {
      return $pol;
    }
    // Now the path is either rootwards, or invalid.
    if ($config['bot_account']) {
      $rootwardsAccount = fetch_policy($config['bot_account'], 'full', FALSE);
      return $rootwardsAccount;

      if ($existing) {
        return $rootwardsAccount;
      }
      if ($orientation->isUpstreamBranch()) {
        cc_message("Attempting to resolve $given_path downstream");
        return $rootwardsAccount;
      }

    }
    cc_log($orientation->isUpstreamBranch());
    cc_violation(UnresolvedAccountnameViolation::create($given_path));

  }

}
