<?php

/**
 * Class for reading and writing policy data from a csv file
 */
class Account {

  /**
   * The unique name of the account
   * @var string
   */
  public $name;

  /**
   * The unixtime the account was created
   * @var string
   */
  public $created;

  /**
   * var bool|null
   */
  public $status;
  /**
   * var float|null
   */
  public $min;
  /**
   * var float|null
   */
  public $max;

  /**
   * @param string $name
   * @param int $created
   * @param bool $status
   * @param float $min
   * @param float $max
   */
  function __construct(string $name, int $created, $status = NULL, $min = NULL, $max = NULL) {
    $this->name = $name;
    $this->created = $created;
    $this->status = $status;
    $this->min = $min;
    $this->max = $max;
  }

  /**
   *
   * @global array $config
   * @param string $name
   * @param int $created
   * @param array $fields
   *   can contain min, max, url and ip
   * @return \static|\RemoteAccount
   */
  static function create(string $name, int $created, array $fields = []) {
    global $config;
    //status is actually a boolean, but we want to confuse false with null.
    $status = isset($fields['status']) ? (int)$fields['status'] : $config['default_status'];
    $min = isset($fields['min']) ? (float)$fields['min'] : NULL;
    $max = isset($fields['max']) ? (float)$fields['max'] :  NULL;

    if (!empty($fields['url'])) {
      $url = $fields['url'];
      $ip = $fields['ip'];
      return new RemoteAccount($name, $created, $status, $min, $max, $url, (string)$ip);
    }
    else {
      return new static($name, $created, $status, $min, $max);
    }
  }

  /**
   * Validate fields prior to creating an account.
   *
   * @param array $fields
   *   $fields, keyed by fieldname;
   * @return array
   *   Errors, keyed by field.
   */
  public static function validateFields(array &$fields) :array {
    $errs = [];
    if (isset($fields['status'])) {
      if ($fields['status'] != 0 and $fields['status'] != 1) {
        $errs['status'] = 'Must be boolean';
      }
    }
    if (isset($fields['min'])) {
      if (!is_numeric($fields['min'])) {
        $errs['min'] = 'must be a number';
      }
    }
    if (isset($fields['max'])) {
      if (!is_numeric($fields['max'])) {
        $errs['max'] = 'must be a number';
      }
    }
    return $errs;
  }

  /**
   * prepare the account for writing to a csv;
   * @param array $headings
   * @return array
   */
  public function toArray(array $headings) :array {
    $output = [];
    foreach ($headings as $prop) {
      $output[$prop] = isset($this->$prop) ? $this->$prop : '';
    }
    return $output;
  }

  /**
   * @param string $view_mode
   * @return class or string
   *   The name or object
   */
  function view(string $view_mode) {
    switch($view_mode) {
      case 'nameonly':
        return $this->name;
      case 'overrides':
        return $this->viewOverridesOnly();
      case 'full':
        return $this->viewWithDefaults();
      default:
        trigger_error('Invalid view mode', E_USER_WARNING);
        return $this->viewWithDefaults();
    }
  }

  /**
   * add default policy values and return an stdClass
   */
  function viewWithDefaults() :stdClass {
    global $config;
    $output = new stdClass();
    foreach (array_keys(get_class_vars(get_class($this))) as $prop) {

      if (is_null($this->$prop)) {
        $def = 'default_'.$prop;
        $output->$prop = $config[$def];
      }
      else {
        $output->$prop = $this->$prop;
      }
    }
    return $output;
  }

  /**
   * return an stdClass with only the name, created and overridden values
   */
  function viewOverridesOnly() :stdClass{
    $output = new stdClass();
    foreach ((array)$this as $key => $val) {
      if (!is_null($val)) {
        $output->$key = $val;
      }
    }
    return $output;
  }

}
