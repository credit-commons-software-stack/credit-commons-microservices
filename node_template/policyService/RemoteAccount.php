<?php

/**
 * Class for reading and writing policy data from a csv file
 */
final class RemoteAccount extends Account {

  /**
   * The remote address of the node.
   * @var string
   */
  public $url;

  /**
   * The remote address of the node.
   * @var string
   */
  private $ip;

  function __construct($name, $created, $status, $min, $max, string $url, string $ip = '') {
    parent::__construct($name, $created, $status, $min, $max);
    $this->url = $url;
    $this->ip = $ip;
  }

  static function validate($url) {
    //not sure how/whether we want to validate this;
  }

  public static function validateFields(array &$fields) :array {
    $errs = parent::validateFields($fields);
    if (isset($fields['url'])) {
      // must be a pure domain or subdomain.
      if (!preg_match('/https?:\/\/[a-z1-9.]+/', $fields['url'])) {
        $errs['url'] = $fields['url'] .'is not a valid url';
      }
      $fields['url'] = strtolower($fields['url']);
    }
    return $errs;
  }

}