<?php

/**
 * policyService
 *
 * Service to add and remove users
 * Each user has min & max limits.
 * They are set through governance.
 * For now they are stored in a text file.
 *
 */
define ('DATA_CSV', 'policy.csv');

require '../common.php';

$policies = createwithCsv(DATA_CSV);

if (!authorise()) {
  cc_response(401);
}
if ($method == 'GET') {
  parse_str($_SERVER['QUERY_STRING'], $params);
  $view_mode = 'full';//default
  extract($params);
  if ($endpoint == 'filter') { // list accounts
    // params should be $status and $view_mode
    $list = [];
    if (!empty($chars)) {
      $policies->filterByName($chars);
    }
    if (isset($status)) {
      $policies->filterByStatus((bool)$status);
    }
    if (isset($local)) {
      $policies->filterByLocal((bool)$local);
    }
    cc_response(200, $policies->view($view_mode));
  }
  elseif ($endpoint == 'fetch') {
    $name = strtolower(urldecode(arg(2)));
    if (isset($policies[$name])) {
      cc_response(200, $policies[$name]->view($view_mode));
    }
    cc_response(404);
  }
}
elseif ($method == 'POST') {
  if ($endpoint == 'join') {
    $name = credcom_json_input()->name;
    if (!is_admin()) {
      cc_response(403); //Here we return the unauthorised response
    }
    $name = strtolower(urldecode($name));
    if (!$policies->validName($name)) {
      respond_invalid_name($name);
    }
    if (!$policies->availableName($name)) {
      respond_duplicate_name($name);
    }
    $a = Account::Create($name, time());
    $policies->addToList($a);
    resaveCsv($policies);
    cc_response(201, $policies[$name]->view('full'));
  }
}
elseif ($endpoint == 'override' and ($method = 'PATCH' or $method = 'PUT')) {
  $name = urldecode(strtolower(arg(2)));
  $fields = (array)credcom_json_input();
  if (isset($policies[$name])) {
    if ($errors = $policies[$name]::validateFields($fields)) {
      cc_response(400, $errors); //@todo Not sure how to pass the errors back
    }
    // be careful with not set and zero values
    $policies[$name]->min = $fields['min']??NULL;
    $policies[$name]->max = $fields['max']??NULL;
    $policies[$name]->status = $fields['status']??NULL;
    $policies[$name]->url = $fields['url']??NULL;

    resaveCsv($policies);
    cc_response(200, $policies[$name]->view('overrides'));
  }
}
cc_response(404);

/**
 * @return boolean
 */

function is_admin() {
  return TRUE;
  cc_response(403);
}

/**
 * Create the policy object from the csv
 */
function createwithCsv() {
  include 'Policy.php';
  $csv = array_map('str_getcsv', file(DATA_CSV));
  $headings = array_shift($csv); //remove the headings.
  return new Policy($headings, array_filter($csv));
}

function resaveCsv($policy) {
  $headings = ['name', 'created', 'status', 'min', 'max', 'url', 'ip'];
  // The UI should ensure that these aren't editable
  $fp = fopen(DATA_CSV, 'w');
  fputcsv($fp, $headings);
  foreach($policy->accounts as $name => $account) {
    fputcsv($fp, $account->toArray($headings));
  }
  fclose($fp);
}

function respond_duplicate_name($name) {
  cc_response(400, "Duplicate $name.", "Duplicate $name.");
}

function respond_invalid_name($name) {
  cc_response(400, "Invalid chars in '$name'", "Invalid chars in '$name'");
}
