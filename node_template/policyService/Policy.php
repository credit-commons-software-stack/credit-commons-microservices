<?php

include 'Account.php';
include 'RemoteAccount.php';

/**
 * Class for reading and writing policy data from a csv file.
 * Performance is not an issue as groups should never exceed more than a few hundred members.
 */
class Policy implements Iterator, ArrayAccess, Countable {

  /**
   *
   * @var array
   */
  public $accounts = [];

  private $pos = 0;

  function __construct(array $headings, array $table) {
    foreach ($table as $row) {
      //$row = array_pad($row, count($headings), '');
      $fields = array_filter(
        array_combine($headings, $row),
        function ($f) {return (bool)strlen($f);}
      );
      $name = $fields['name'];
      $a = Account::Create($name, $fields['created'] ?? time(), $fields);
      $this->addToList($a);
    }
  }

  function addToList(Account $a) {
    $this->accounts[$a->name] = $a;
  }

  /**
   *
   * @param string $string
   */
  function filterByname(string $string = '') {
    $this->accounts = array_filter($this->accounts, function ($a) use ($string) {
      return empty($string) or is_int(stripos($a->name, $string));
    });
  }

  /**
   *
   * @param bool $status
   *   True for active, FALSE for Blocked
   */
  function filterByStatus(bool $status) {
    global $config;
    $this->accounts = array_filter($this->accounts, function ($a) use ($status, $config) {
      return is_null($a->status) ? ($status == $config['default_status']) : $status == $a->status;
    });
  }
  /**
   *
   * @param bool $local
   *   TRUE for local accounts, FALSE for remote accounts
   */
  function filterByLocal(bool $local) {
    $this->accounts = array_filter($this->accounts, function ($a) use ($local) {
      return (bool)$a->url != $local;
    });
  }

  /**
   * @param string $view_mode
   * @return stdClass[]
   */
  function view(string $view_mode) : array {
    return array_map(
      function ($a) use ($view_mode) {return $a->view($view_mode);},
      $this->accounts
    );
  }


  function availableName($name) {
    return !isset($this[$name]) ;
  }

  function validName($name) {
    return preg_match('/^[a-z0-9@.]{1,32}$/', $name) and strlen($name) < 32;
  }

  static function validateFields(array $fields) : array {
    $errs = RemoteAccount::validateFields($fields);
    return $errs;
  }

  function key() {
    return $this->pos;
  }

  function valid() {
     return isset($this->accounts[$this->pos]);
  }

  function current() {
    return $this->accounts[$this->pos];
  }

  function rewind() {
    $this->pos = 0;
  }

  function next() {
    ++$this->pos;
  }

  public function offsetExists($offset) : bool {
    return array_key_exists($offset, $this->accounts);
  }

  public function offsetGet($offset) {
    return $this->accounts[$offset];
  }
  public function offsetSet($offset, $value) : void {
    $this->accounts[$offset] = $value;
  }
  public function offsetUnset($offset) : void {
    trigger_error('Cannot delete accounts', E_USER_WARNING);
  }
  public function count() : int {
    return count($this->accounts);
  }
}
