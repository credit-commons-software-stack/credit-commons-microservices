<?php

/**
 * Business logic service
 *
 * @todo this is just a stub. This service needs both a format for rules and
 * storage.
 */
require '../common.php';

if ($endpoint == 'append') {
  $entry = credcom_json_input();
  $additional = [];
  if (arg(2) == 'bill' or arg(2) == 'credit') {
    if ($config['payee_fee']) {
      $additional[] = payee_fee($entry);
    }
    if ($config['payer_fee']) {
      $additional[] = payer_fee($entry, $config['payer_fee']);
    }
  }
  cc_response(200, $additional);
}
elseif ($endpoint == 'rules') {
  parse_str($_SERVER['QUERY_STRING'], $params);
  if ($params['full']) {
    $result = [
      'fees' => [
        '_note' => 'no format has been decided for transmitting rules',
        'description' => "Payer and payee should pay 1 to the admin account on every transaction of type 'default'"
      ]
    ];
  }
  else {
    $result = ['fees'];
  }
  cc_response(200, $result);
}

/**
 * Charge the payee.
 */
function payee_fee(stdClass $entry) : stdClass {
  global $node_name, $config;
  // Might want to author with the authenticaed account rather than $fees account
  $fee = calc($entry->quant, $config['payee_fee']);
  cc_message ($entry->payee .' will pay a fee of '. $fee);
  return (object)[
    'payer' => $entry->payee,
    'payee' => $config['fees_account'],
    'author' => $config['fees_account'],
    'quant' => $fee,
    'description' => 'payee fee of '.$config['payee_fee'].' to '.$node_name
  ];
}

/**
 * Charge the payer.
 */
function payer_fee(stdClass $entry) : stdClass {
  global $node_name, $config;

  $fee = calc($entry->quant, $config['payer_fee']);
  cc_message ($entry->payer .' will pay a fee of '. $fee);
  // Might want to author with the authenticated account rather than $fees account
  return (object)[
    'payer' => $entry->payer,
    'payee' => $config['fees_account'],
    'author' => $config['fees_account'],
    'quant' => $fee,
    'description' => 'payer fee of '.$config['payer_fee'].' to '.$node_name
  ];
}

/**
 *
 * @param type $quant
 * @param type $fee
 * @return float
 */
function calc($quant, $fee) {
  preg_match('/([0-9.])(%?)/', $fee, $matches);
  $num = $matches[1];
  $percent = $matches[2];
  if ($percent) {
    $val = (float)$quant * $num/100;
  }
  else {
    $val =  (float)$num;
  }
  return (float)$val;
}
