# Non-Disclosure agreement
## Credit Commons Protocol 

### Technical Stack Outline  

---------------------------------------------------------------------------------

### Preamble
Why an NDA? This will (should) be the first question you ask. If this is a commons, then surely it should be open to all, open-source?

Good question.

The answer is that if this is a commons, then there are commoners, with rights and responsibilities, and non-commoners, with fewer rights. A commons has a boundary.



The work that has been done on the development of the outline provided in the original Credit Commons white-paper is potentially precious.

Not precious in terms of money, but precious in terms of the value to humanity of a viable means-of-exchange system that operationalises and enshrines trust as the empowering mechanism of a global civilisation rooted in locality (valuing equally both physical and ontological locality).

What we have right now, though, is a seed, a sketch, nothing more. 

Our second duty is to nurture this seed; protect it, help it grow, develop, become strong. Strong enough to compete in a forest that is already full of aggressive and greedy giants.

If we are to do that, this small idea must become resilient, capable, robust, useful and strongly recognisable as a nameable thing - a Credit Commons. This builds a protection much stronger than any legal force - a strong cultural identity.

If we don't treat this seed as precious, but simply strew it far and wide in the hope that it will grow somewhere, we are likely to see all manner of straggly weeds and bizarre mutations, as projects are developed without our vision.

The worst outcome, of course, would be to see a tall, strong version of this thing which has been mutated to serve profit extraction, and which moves faster and with more certainty - because it can easily attract investment, because it has a simplistic measure of success - shareholder value - and nothing else.

Examples abound of just this process happening to wide-eyed optimistic projects - http/html, the idea of the sharing economy, Facebook's Libra. Open-source inventions thrown, with fanfare, to the wind, followed by extractive and self-serving 'embrace and extend'.

At present, we can't be more specific about how we will protect the project from enclosure. We do understand, of course, that to succeed, a large part of what we build must become open-source (our vision, clear as it is, may not be the optimal one - we must let others be free to try alternate approaches, to improve). 

But until we understand the project itself in more detail, until we can undertake work to assess specific threats and how they might be countered, until we can thoroughly understand appropriate licensing, and while the condition of privity is not detrimental to the development of the project, we believe it is the correct choice to keep this as a members-only commons. 

That's why this is an NDA.

### MUTUAL NON DISCLOSURE AGREEMENT

This Agreement is made on the :

Between:

Credit Commons Protocol Association 
of 4 Beechdale Road, London, SW2 2BE


And

of 

WHEREAS: The two Parties are prepared to disclose confidential information to each other for the purposes of establishing joint projects, joint commercial ventures or for advisory purposes.

NOW IT IS HEREBY AGREED: Either Party, when acting as the recipient of information from the other, is prepared to accept disclosure of confidential information on the following terms and conditions.

1. Confidential information means any information supplied by either Party to the other, whether written, graphic or oral, of a technical, statistical or commercial nature and including samples or specimens but excluding:

a)    Information which at the time of disclosure is in the public domain

b)    Information which after disclosure becomes part of the public domain through no fault of the recipient

c)    Information received by either Party from any third party having the right to disclose the same, provided that the information was not obtained improperly

d)    Information which was in the recipient’s possession prior to disclosure.

2. The recipients agree to:

a)    Use the information only for the purposes described above, and for which purpose it may be disclosed only to those employees, associates or consultants deemed necessary for its proper evaluation who are bound to the recipient by similar obligations of confidentiality

b)    Hold in confidence all confidential information disclosed for a period of five years from the date of disclosure

c)    Not otherwise disclose the information to any person, firm or company without prior written permission of the donor Party

3. The disclosure of information shall not be construed as conveying any rights in the information to the recipient or any obligation on either of the two Parties to enter further agreements.

4. No modification or waiver of this agreement shall be valid unless in writing and signed by both Parties.

5. This agreement shall be governed according to the Laws of England and the Parties shall submit to the jurisdiction of the English Courts.

As WITNESS the hands of the parties thereto:

Signed:                     

On behalf of Credit Commons Protocol Association by Dil Green



Signed:                      



